class AuthenticateUser

#TEMPORARY Incorporate username after tests are done
  def initialize(email, password)
    @email = email
    @password = password
  end

  # Service entry point
  def call
    Auth.encode(user_id: login_credential.user_id) if login_credential
  end

  private

  attr_reader :email, :password

  # verify user credentials
  def login_credential
    login_credential = LoginCredential.find_by(email: email)
    return login_credential if login_credential && login_credential.authenticate(password)
    # raise Authentication error if credentials are invalid
    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end
end