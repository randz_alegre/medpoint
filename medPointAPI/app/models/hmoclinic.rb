class Hmoclinic < ApplicationRecord
	belongs_to :clinic
	belongs_to :hmo

	validates :clinic_id, presence: true
	validates :hmo_id, presence: true
end
