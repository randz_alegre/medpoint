class VenueAdmin < ApplicationRecord
  belongs_to :user
  belongs_to :clinic

  validates :user_id, presence: true, null: false 
  validates :clinic_id, presence: true, null: false

  def self.add_admin(current_user,params)
    is_approved = false
    if current_user.is_admin?
      is_approved = true
    end
    venue_admin = VenueAdmin.new(user_id:params[:user_id],clinic_id:params[:clinic_id],is_approved: is_approved)

    if venue_admin.save
      return {message: Message.created}  
    else
      return {message: Message.something_wrong}
    end
  end

  def self.remove_admin(user,params)
    venue_admin = VenueAdmin.where("id = ? AND clinic_id = ? ",params[:venue_admin_id],params[:clinic_id]).first
    if venue_admin != nil and (user.is_admin? or user.id == venue_admin.user_id)
      begin
        venue_admin.delete
        return {message: Message.deleted}
      rescue
        return {message: Messagee.something_wrong}
      end
    else
      return {message: Message.not_allowed}
    end
  end

  def self.get_requests(user,clinic_id)
    if user.is_admin?
      venue_admins = VenueAdmin.joins(:user, :clinic).select('"venue_admins".id as id,"users".id as user_id, "clinics".id as clinic_id,"users".first_name,"users".last_name,"clinics".clinic_name').where(is_approved: false).where(clinic_id: clinic_id)
      return {requests: venue_admins} 
    else
      return {message: Message.not_allowed}
    end
  end

  def self.approve_request(user,id)
    if user.is_admin?
      venue_admin = VenueAdmin.find(id)
      venue_admin.is_approved = true
      if venue_admin.save
        return {message: Message.request_success}
      else 
        return {message: Message.something_wrong}
      end
    else
      return {message: Message.not_allowed}
    end
  end
  def self.get_admins(user,clinic_id)
    if user.is_admin?
      venue_admins = VenueAdmin.joins(:user, :clinic).select('"venue_admins".id as id,"users".id as user_id, "clinics".id as clinic_id,"users".first_name,"users".last_name').where(clinic_id: clinic_id)
      return {venue_admins: venue_admins} 
    else
      return {message: Message.not_allowed}
    end
    return venue_admins
  end
end
