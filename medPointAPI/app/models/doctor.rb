class Doctor < ApplicationRecord
  belongs_to :user
  has_many :doctor_specialties
  has_many :specialties, through: :doctor_specialties
  has_many :schedules
  
  
  validates :user_id, :presence => true, :uniqueness => true
  validates :license_id, :presence => true, :uniqueness => true

end
