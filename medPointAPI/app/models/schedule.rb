class Schedule < ApplicationRecord
  belongs_to :clinic
  belongs_to :doctor

  #change isActive syntax format in the future
  def self.add_doctor_schedule(current_user, params)
    if clinic.exists?(params[:clinic_id])
      is_approved = false
      is_admin = current_user.is_admin?
      is_venue_admin =  current_user.is_venue_admin?(params[:clinic_id])
      is_doctor = Doctor.exists?(user_id: current_user.id)
      proceed = false
      if is_admin or is_venue_admin 
        is_approved = true
        proceed = true
      elsif is_doctor
        proceed = true      
        if proceed
          doctor_exists = false
          if is_doctor or Doctor.exists?(doctor_id: params[:doctor_id])
            doctor_exists = true
          end 
          if doctor_exists
            schedule = Schedule.build(
              clinic_id: params[:clinic_id],
              doctor_id: doctor_id,
              frequency: params[:frequency],
              start_time: params[:start_time],
              end_time: params[:end_time],
              timezone: params[:timezone],
              isActive: is_approved
            )
            if schedule.save
              return { message: Message.created, status: 200}
            end
          end
        else
          return { message: Message.not_allowed, status: 500}
        end
        return { message: Message.something_wrong, status: 500}
      end
    end
  end
  
  def self.get_requests(user,clinic_id)
    if user.is_admin? or user.is_venue_admin?(id)
      schedules = Schedule.where(isActive: false, clinic_id: clinic_id)
      return {schedules: schedules}
    else
      return {message: Message.not_allowed}
    end
  end
 
  def self.approve_request(user,id)
    if user.is_admin? or user.is_venue_admin?(id)
      schedule = Schedule.find(id)
      schedule.isActive = true
      if schedule.save
        return { message: Message.request_success}
      else
        return { message: Message.something_wrong}
      end
    else
      return { message: Message.not_allowed}
    end
  end
end
