class Appointment < ApplicationRecord
  belongs_to :schedule
  belongs_to :user
  attr_accessor :status, :proposed_schedule
end
