class Userhmo < ApplicationRecord
	belongs_to :user
	belongs_to :hmo

	validates :user_id, presence: true
	validates :hmo_id, presence: true
	validates_uniqueness_of :user_id
	validates_uniqueness_of :hmo_id
end
