class Clinic < ApplicationRecord
  has_many :hmoclinics
  has_many :hmos, -> { distinct }, :through => :hmoclinics
  has_many :schedules
  has_many :venue_admin	
  geocoded_by :clinic_full_address

  validates :establishment_type, presence: true, length: { maximum: 50 }	
  validates :clinic_name, presence: true, length: { maximum: 200 }	
  validates :city, presence: true, length: { maximum: 50 }	
  validates :province, presence: true, length: { maximum: 50 }	
  validates :telephone_number, presence: true, length: { maximum: 200 }	
  validates :mobile_number, presence: true, length: { maximum: 50 }	
  validates :street_address, presence: true, length: { maximum: 200 }	
  validates :floor_level, presence: true, length: { maximum: 20 }
  validates :bldg_name, presence: true, length: { maximum: 250 }
  validates :barangay, presence: true, length: { maximum: 50 }		


  after_validation :geocode, :if => :bldg_name_changed?


  def clinic_full_address
    [bldg_name, street_address, barangay, city, province].compact.join(', ')
  end

  def self.import(file)

    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      row["street_address"].downcase!
      row["clinic_name"].downcase!
      row["city"].downcase!
      row["bldg_name"].downcase!
      row["province"].downcase!  
      row["establishment_type"].downcase!

      Clinic.create! row.to_hash
    end
  end

  def self.create_clinic(user, params)
    is_approved = false
    
    if user.is_admin?
      is_approved = true
    end
    clinic = Clinic.new(params)

    clinic.clinic_name.downcase!
    clinic.city.downcase!
    clinic.bldg_name.downcase!
    clinic.street_address.downcase!
    clinic.province.downcase!  
    clinic.establishment_type.downcase!
	clinic.is_approved = is_approved
    if clinic.save
      return clinic, status: :created
    else
      return elinic.errors, status: :unprocessable_entity
    end
  end
  
  # To test
  def self.get_requests(user)
    if user.is_admin?
      clinics = Clinic.where(is_approved: false)
      return {requests: clinics} 
    else
      return {message: Message.not_allowed}
    end
  end

  # To test
  def self.approve_request(user,id)
    if user.is_admin?
      clinic = Clinic.find(id)
      clinic.is_approved = true
      if clinic.save
        return {message: Message.request_success}
      else 
        return {message: Message.something_wrong}
      end
    else
      return {message: Message.not_allowed}
    end
  end
end
