class LoginCredential < ApplicationRecord
  belongs_to :user
  
  has_secure_password
  
  before_validation :downcase_email
  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, :presence => true, :uniqueness => true,
                      :format => EMAIL_REGEX
  validates :password_digest, confirmation: true, length: { minimum: 10  }, :if => :password
  validates :username, presence: true, uniqueness: true
  
  def downcase_email
    self.email = email.downcase if email.present?
  end
end
