class Specialty < ApplicationRecord
  has_many :doctor_specialties
  has_many :doctors, through: :doctor_specialties
  
  validates :specialty_name, presence: true
  validates :specialty_desc, presence: true
  
  #attr_accessor :specialty_name, :specialty_desc 
end
