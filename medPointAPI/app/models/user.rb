class User < ApplicationRecord
  has_one :userhmo
  has_one :hmo, :through => :userhmo
  has_one :login_credential, dependent: :destroy
  has_many :contact_numbers
  has_one :doctor, dependent: :destroy
  has_many :venue_admin

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :birthdate, presence: true

  "
  def generate_password_token!


  end

  def password_token_valid?
    (self.reset_password_sent_at + 4.hours) > Time.now.utc
  end

  def reset_password!(password)
    self.reset_password_token = nil
    self.password = password
    save!
  end

  def generate_token
    SecureRandom.hex(13)
  end"
  def self.create(user_params, login_params)
    @user = User.new(user_params)


    begin
      if @user.save
        @login_credential = LoginCredential.new(
          user_id: @user.id,
          email: login_params[:email],
          password: login_params[:password],
          username: login_params[:username]
        )
        if @login_credential.save
          @current_user = @user
          @jwt = AuthenticateUser.new(login_params[:email],login_params[:password]).call
          return {
            jwt: @jwt,
            message: Message.account_created,
            user_id: @user.id,
            email: @login_credential.email, 
            first_name: @user.first_name,
            last_name: @user.last_name,
            birthdate: @user.birthdate,
            status: 201
          }
        else 
           @user.delete
        end
      end
    rescue => e 
      logger.error e.message
      logger.error e.backtrace.join("\n")
      return false
    end
  end

  def self.create_doctor(user_id, doctor_params)

    @doctor = Doctor.create!(
      user_id: user_id,
      license_id: doctor_params[:license_id],
      issue_date: doctor_params[:issue_date],
      is_verified: false
    )
    return { 
      message: Message.account_upgraded,
      status: 200
    }
  end

  def is_doctor?
    Doctor.exists?(user_id: self[:id])
  end
   
  def is_venue_admin?(clinic_id)
    VenueAdmin.exists?(user_id: self[:id], clinic_id: clinic_id)
  end

  def get_doctor_id
    if Doctor.exists(user_id: self[:id])
      Doctor.find(user_id: self[:id]).id
    else
      return 0
    end
  end

  def is_admin?
    self[:user_type].to_s == Role.admin
  end

end
