class DoctorSpecialty < ApplicationRecord
  belongs_to :doctor
  belongs_to :specialty
  attr_accessor :certification_date, :certification_id
end
