class ContactNumber < ApplicationRecord
  belongs_to :user

  validates :area_code, presence: true
  validates :number, presence: true
  validates :user_id, presence: true


  def self.add(user_id, params)
    begin 
      ContactNumber.create!(user_id: user_id,
                            area_code: params[:area_code],
                            number: params[:number])
      return true
    rescue => e
      logger.error e.message
      return false
    end
  end  
end
