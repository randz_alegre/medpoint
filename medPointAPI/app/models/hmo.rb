class Hmo < ApplicationRecord
	has_many :hmoclinics
	has_many :clinics, -> { distinct }, :through => :hmoclinics

	has_many :userhmos
	has_many :users, -> { distinct }, :through => :userhmos

	validates :hmo_name, presence: true, length: { maximum: 100 }, uniqueness: true	
	validates :hmo_contact_number, presence: true, length: { maximum: 100 }	
	validates :hmo_website, presence: true, length: { maximum: 100 }	
	validates :hmo_hotline, presence: true, length: { maximum: 500 }	
	validates :hmo_logo, presence: false, length: { maximum: 100 }	
	validates :complete_address, presence: true, length: { maximum: 200 }


	def self.import(file)

		spreadsheet = open_spreadsheet(file)
		header = spreadsheet.row(1)
		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			
			row["hmo_name"].downcase!
	        row["hmo_website"].downcase!
	        row["hmo_logo"].downcase!  
	        row["complete_address"].downcase!

			Hmo.create! row.to_hash
		end
	end

	
end
