class Message
  def self.not_found(record = 'record')
    "Sorry, #{record} not found."
  end

  def self.invalid_credentials
    'Invalid credentials'
  end

  def self.invalid_token
    'Invalid token'
  end

  def self.missing_token
    'Missing token'
  end

  def self.unauthorized
    'Unauthorized request'
  end

  def self.account_created
    'Account created successfully'
  end

  def self.account_not_created
    'Account could not be created'
  end

  def self.contact_number_deleted
    'Contact Number was deleted from your profile'
  end 

  def self.contact_number_created
    'Contact Number added to your profile'
  end 

  def self.contact_number_not_created
    'There are issues regarding the addition of the contact number'
  end

  def self.account_upgraded
    'Account upgraded to Doctor successfully'
  end

  def self.expired_token
    'Sorry, your token has expired. Please login to continue.'
  end

  def self.not_allowed
    'Sorry, there is no such feature'
  end

  def self.specialty_created
    'Specialty created successfully'
  end

  def self.created
    'Created Record'
  end

  def self.updated
    'Successfully Updated'
  end
  
  def self.deleted
    'Successfully Deleted'
  end

  def self.something_wrong
    'Something went terribly wrong'
  end

  def self.request_success
    'Operation Successful'
  end

end
