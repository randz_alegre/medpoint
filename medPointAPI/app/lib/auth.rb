require 'jwt'

class Auth

	ALGORITHM = "HS256"	
	HMAC = Rails.application.secrets.secret_key_base
  def self.issue(payload)
  	JWT.encode(
      payload,
      HMAC,
      ALGORITHM, { :email => "testing@gmail.com" }
    )
  end

  def self.encode(payload, exp = 24.hours.from_now)
	payload[:exp] = exp.to_i
	JWT.encode(payload,
	HMAC,
	ALGORITHM
	)
  end
  
  def self.decode(token)
    begin
      body = JWT.decode(token, 
	           HMAC, 
               true, 
			   {algorithm: ALGORITHM}
			 ).first
	  HashWithIndifferentAccess.new body
    rescue JWT::ExpiredSignature, JWT::VerificationError => e
	  # raise custom error to be handled by custom handler
	  raise ExceptionHandler::ExpiredSignature, e.message
    end  
  end

end