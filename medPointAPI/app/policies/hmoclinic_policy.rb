class HmoclinicPolicy < ApplicationPolicy
	def create?
		isUserAdmin
	end

	def update?
		isUserAdmin
	end

	
end
