class ClinicPolicy < ApplicationPolicy

def create?
	isUserAdmin
end

def update?
	isUserAdmin
end

def destroy?
	isUserAdmin
end

def importclinics?
	isUserAdmin
end

private
	def isUserAdmin
		user.user_type == 'superUser'
	end
end