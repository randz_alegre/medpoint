class HmoPolicy < ApplicationPolicy

	def create?
		isUserAdmin
	end

	def update?
		isUserAdmin
	end

	def destroy?
		isUserAdmin
	end

	def importhmos?
		isUserAdmin
	end

end