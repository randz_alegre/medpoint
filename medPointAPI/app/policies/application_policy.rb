class ApplicationPolicy
	attr_reader :user, :resource

	def initialize(user, resource)
		@user = user
		@resource = resource
	end

	private
		def isUserAdmin
			user.user_type == 'superUser'
		end
end