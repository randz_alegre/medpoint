module V1
  
    class HmoclinicsController < ApplicationController
      before_action :set_hmoclinic, only: [:update]
      after_action :verify_authorized, :only => [:create, :update]

      # POST /hmoclinics
      def create
        authorize Hmoclinic
        @hmoclinic = Hmoclinic.new(hmoclinic_params)

        if @hmoclinic.save
          render json: @hmoclinic, status: :created, location: @hmoclinic
        else
          render json: @hmoclinic.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /hmoclinics/1
      def update
        authorize Hmoclinic
        if @hmoclinic.update(hmoclinic_params)
          render json: @hmoclinic
        else
          render json: @hmoclinic.errors, status: :unprocessable_entity
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_hmoclinic
          @hmoclinic = Hmoclinic.find(@current_user.id)
        end

        # Only allow a trusted parameter "white list" through.
        def hmoclinic_params
          params.require(:hmoclinic).permit(:clinic_id, :hmo_id)
        end
    end

end  
