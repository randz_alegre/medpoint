module V1
	class SubscriptionsController < ApplicationController
		skip_before_action :authenticate, :only => [:create, :update], raise: false
		def index
			
		end

		def create
			subscription = Subscription.new(subscriber_params)

			if subscription.save
				render json: subscription, status: :created, location: subscription
			else
				render json: subscription.errors, status: :unprocessable_entity
			end
		end

		def update
		
		end

		private

		def subscriber_params
			params.require(:subscription).permit(:email)
		end
	end
end
