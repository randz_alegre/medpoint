module V1
  class SchedulesController < ApplicationController
    before_action :set_schedule, only:[:show, :update,:destroy]

    def index
      json_response({schedules: Schedule.where(clinic_id: params[:clinic_id], isActive: true)})
    end

    # POST doctor/:doctor_id/schedule
    def create
      json_response(Schedule.add_doctor_sched(schedule_params)) 
    end

    # GET /schedule/:id
    def show
      json_response(@schedule)
    end

    # PUT /schedule/:id
    def update
      if @current_user.is_admin? or @current_user.is_venue_admin?(params[:clinic_id]) or @current_user.id.to_i == @schedule.doctor_id
        @schedule.update(schedule_params)
        json_response({message: Message.updated})
      else
        json_response({message: Message.not_allowed}, :NotFound)
      end
    end

    # DELETE /schedule/:id
    def destroy 
      if @current_user.is_admin? or @current_user.is_venue_admin?(params[:clinic_id]) or @current_user.id.to_i == @schedule.doctor_id
        @schedule.destroy
        json_response({message: Message.deleted})
      else
        json_response({message: Message.not_allowed}, :NotFound)
      end
    end

    def get_schedule_requests
      json_response(Schedule.get_requests(@curren_user,params[:clinic_id]))
    end

    def approve_schedule_request
      json_response(Schedule.approve_request(@current_user,params.require(:schedule_id)))
    end

    private

    def schedule_params
      params.require(:schedule).permit(:schedule_id, :doctor_id,:clinic_id,:freqnency,:start_time,:end_time,:timezone)
    end

    def set_schedule
      @schedule = Schedule.find(params[:id])
    end
  end
end
