module V1

      class HmosController < ApplicationController
        after_action :verify_authorized, :only => [:create, :update, :importhmos]
        before_action :set_hmo, only: [:show, :update]



        def importhmos
          authorize Hmo
          Hmo.import(params[:file])
          render json: { status: :created }
        end
        # GET /hmos
        def index

          if params[:clinic_id].present?
            hmoList = Clinic.find_by("id = ?", params[:clinic_id])
            @hmos = hmoList.hmos
          elsif params[:user_id].present?
             @hmos = @current_user.hmos.includes(:userhmos).where("userhmos.user_id = ? AND userhmos.isDeleted = ?", @current_user.id, false).references(:userhmos)

          else
            @hmos = Hmo.all
          end

          

          render json: @hmos
        end

        # GET /hmos/1
        def show
          render json: @hmo
        end

        # POST /hmos
        def create
          authorize Hmo
          @hmo = Hmo.new(hmo_params)

          if @hmo.save
            render json: @hmo, status: :created, location: @hmo
          else
            render json: @hmo.errors, status: :unprocessable_entity
          end
        end

        # PATCH/PUT /hmos/1
        def update
          authorize Hmo
          if @hmo.update(hmo_params)
            render json: @hmo
          else
            render json: @hmo.errors, status: :unprocessable_entity
          end
        end

        private
          # Use callbacks to share common setup or constraints between actions.
          def set_hmo
            @hmo = Hmo.find(params[:id])
          end

          # Only allow a trusted parameter "white list" through.
          def hmo_params
            params.require(:hmo).permit(
              :hmo_name, 
              :complete_address,
              :hmo_contact_number,
              :hmo_website,
              :hmo_hotline,
              :hmo_logo)
          end
      end

end
