module V1

  class ClinicsController < ApplicationController
    skip_before_action :authorize_request, :only => [:index]
    
    before_action :set_clinic, only: [:show, :update]

    def importclinics
      authorize Clinic
      Clinic.import(params[:file])
      render json: { status: :created }
    end

    # GET /clinics
    def index
      if params[:hmo_id].present?
        hmo = Hmo.find_by("id = ?", params[:hmo_id])
        @clinics = hmo.clinics
        #@clinics = Hmo.clinics.includes(:hmoclinics).where("hmoclinics.hmo_id = ?", params[:hmo_id]).references(:hmoclinics)
      else
        @clinics = Clinic.all
      end
      render json: @clinics
    end

    def searchbycity
      if params[:city].present? && params[:searchFor].present?

        clinics = Clinic.where("establishment_type = ? AND city = ?", params[:searchFor].downcase, params[:city].downcase)

        render json: clinics

      else

        render json: { error: 404, status: :not_found }

      end

    end

    def citylist
      if params[:selectedProvince].present?
        cities = Clinic.select(:city).where("province = ?", params[:selectedProvince]).distinct  
        render json: cities
      else
        render json: { error: 404, status: :not_found }
      end
    end

    def searchnearby
      radius = 5 #default radius of the area.
      if params[:latitude].present? && params[:longitude].present?

        latitude = params[:latitude].to_f
        longitude = params[:longitude].to_f

        if @current_user.userhmo != nil
          userHMO = @current_user.userhmo.hmo

          if !@current_user.userhmo.isDeleted
            clinics = userHMO.clinics.near([latitude,longitude], radius)
            render json: clinics
          else
            clinics = Clinic.near([latitude,longitude], radius)
            render json: clinics
          end

        else
          clinics = Clinic.near([latitude,longitude], radius)
          render json: clinics
        end
      else
        render json: { error: 404, status: :not_found }
      end
    end

    # GET /clinics/1
    def show

      render json: @clinic

    end


    # POST /clinics
    def create
      json_response(Clinic.create_clinic(@current_user, clinic_params))
    end

    # PATCH/PUT /clinics/1
    def update
      if @current_user.is_admin? or @current_user.is_venue_admin?(params[:clinic_id])
        if @clinic.update(clinic_params)
          render json: @clinic
        else
          render json: @clinic.errors, status: :unprocessable_entity
        end
	  else
	    json_response({message: Message.not_allowed})
	  end
    end

	def get_clinic_requests
	  json_response(Clinic.get_requests(@current_user))
	end
	
	def approve_clinic_request
	  json_response(Clinic.approve_request(@current_user,params[:clinic_id]))
	end
	
    # POST /clinics/1/addAdmin    
    def add_admin
      json_response(VenueAdmin.add_admin(@current_user,venue_admin_params))
    end
    
    def remove_admin
      # User can only delete if user is System admin or the admin of the clinic
      json_response(VenueAdmin.remove_admin(@current_user, venue_admin_params))
    end    

    def get_admin_requests
      json_response(VenueAdmin.get_requests(@current_user,params[:clinic_id]))
    end

    def approve_admin_request
      json_response(VenueAdmin.approve_request(@current_user,params.require(:venue_admin_id))) 
    end

    def get_admins
      json_response(VenueAdmin.get_admins(@current_user,params[:clinic_id]))
    end
    private
    # Use callbacks to share common setup or constraints between actions.
    def set_clinic
      @clinic = Clinic.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def clinic_params
      params.require(:clinic).permit(
        :establishment_type,
        :clinic_name,
        :coordinator,
        :asst_coordinator,
        :floor_level,
        :bldg_name,
        :street_address,
        :barangay,
        :city,
        :province,
        :telephone_number,
        :mobile_number,
        :longitude,
        :latitude
      )
    end

    def venue_admin_params
      params.permit(:venue_admin_id, :user_id, :clinic_id)
    end
  end
end
