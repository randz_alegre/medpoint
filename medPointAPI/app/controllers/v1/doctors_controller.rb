module V1
	class DoctorsController < ApplicationController
	  before_action :set_doctor, only: [:show, :update, :destroy]

    def index
      @doctors = Doctor.all
      json_response({doctors_list: @doctors})
    end

    #add query for doctors here
    #
    #
    #
    #
    #
    
	  private

	  def doctor_params
		# whitelist params
		params.permit(:user_id, :license_id, :issue_date, :is_verified)
	  end

	  def set_doctor
		@doctor = Doctor.find(params[:id])
	  end

	end
end
