module V1
	
	class UserhmosController < ApplicationController
		before_action :set_userhmo, only: [:update, :destroy]

		def create
			userHMO = Userhmo.new(userhmoParams)
			userHMO.user_id = @current_user.id
			
			if userHMO.save
	          render json: userHMO, status: :created, location: userHMO
	        else
	          render json: userHMO.errors, status: :unprocessable_entity
	        end
		end

		def update
			if @userHMO.update(userhmoParams)
				render json: @userHMO
	        else
	          	render json: @userHMO.errors, status: :unprocessable_entity
	        end	
		end

		private

		def set_userhmo
          @userHMO = Userhmo.find_by("user_id = ? AND id = ?", @current_user.id, params[:id])
        end

		def userhmoParams
			params.require(:userhmo).permit(:hmo_id, :isDeleted)
		end
	end
end
