module V1

  class UsersController < ApplicationController


    skip_before_action :authorize_request, :only => [:create, :create_doctor, :authenticatelogin, :forgotpassword, :resetpassword], raise: false

    wrap_parameters :user, include: [:first_name, :last_name, :birthdate, :user_type]
    # GET /users/
    def index 
      #Only show data of current user
      doctor_data = Doctor.find_by(user_id: @current_user.id)
      contact_numbers = ContactNumber.where(user_id: @current_user.id)
      json_response({user: @current_user, license: doctor_data, contact_details: contact_numbers})
    end
=begin
        def forgotpassword
          if params[:email].blank?
            return render json: {error: 'Email not present'}
          end

          user = User.find_by(email: params[:email].downcase)

          if user.present?
            user.update_column(:reset_password_token, user.generate_token)
            user.update_column(:reset_password_sent_at, Time.now.utc)
            # SEND EMAIL HERE
            render json: {status: 'ok'}, status: :ok
          else
            render json: {error: ['Email address not found. Please check and try again.']}, status: :not_found
          end
        end


        def resetpassword
          token = params[:token].to_s

          if params[:email].blank?
            return render json: {error: 'email not present'}
          end

          user = User.find_by(reset_password_token: token)

          if user.present? && user.password_token_valid?
            if user.reset_password!(params[:password])
              render json: {status: 'ok'}, status: :ok
            else
              render json: {error: user.errors.full_messages}, status: :unprocessable_entity
            end
          else
            render json: {error:  ['Link not valid or expired. Try generating a new link.']}, status: :not_found
          end
        end


        def updatepassword
          if !params[:password].present?
            render json: {error: 'Password not present'}, status: :unprocessable_entity
            return
          end

          if current_user.reset_password(params[:password])
            render json: {status: 'ok'}, status: :ok
          else
            render json: {errors: current_user.errors.full_messages}, status: :unprocessable_entity
          end
        end


        def authenticatelogin
          user = User.find_by("email = ?",params[:email])


          if user && user.authenticate(params[:password])
              jwt = Auth.issue({user: user.id})

              render json: {
                jwt: jwt, 
                id: user.id,
                email: user.email, 
                first_name: user.first_name,
                last_name: user.last_name,
                birthdate: user.birthdate,
                status: 200
              }
          else

              render json: {error: "Not Found"}, status: 404
          end


        end
=end
    # POST /signup
    def create
      response =  User.create(user_params, login_params)
      if response

        ContactNumber.add(response[:user_id],contact_number_params) if contact_number_params      
        json_response response
      else
        json_response({message: Message.account_not_created},:failed) 
      end
    end

    # POST /doctorsSignup
    def create_doctor
      response =  User.create(user_params, login_params)
      if response
        user_id = response[:user_id]
        User.create_doctor(user_id,doctor_params)
        ContactNumber.add(user_id,contact_number_params) if contact_number_params      
        json_response response
      else  
        json_response({message: Message.account_not_created},:failed) 
      end
    end

    #POST /iAmADoctor
    def upgrade_to_doctor
      begin
        response = User.create_doctor(@current_user.id,doctor_params)
        json_response response
      rescue => e
        logger.error e.message
        logger.error e.backtrace.join("\n")
        json_response(e, :unprocessable_entity)
      end
    end 

    # POST /contactNumber
    def add_contact_number
      response =  ContactNumber.add(@current_user.id, contact_number_params) if contact_number_params      
      if response
        json_response({message: Message.contact_number_created}) 
      else 
        json_response({message: Message.contact_number_not_created}, :failed)  
      end
    end

    # DELETE /contactNumber
    def delete_contact_number
      id = contact_number_params[:contact_number_id]
      if id
        contact_number = ContactNumber.find(id)
        if contact_number && contact_number.user_id == @current_user.id
          contact_number.delete
          json_response({message: Message.contact_number_deleted}) 
        end
      end       
    end

    # GET /contactNumbers
    def contact_numbers
      @contact_numbers = ContactNumber.where(user_id: @current_user.id)
      json_response @contact_numbers   
    end

    # PATCH/PUT /users/1
    def update

      if params[:user][:password].blank?
        params[:user].delete(:password)
      end

      @current_user.update_attributes(user_params)

      if @current_user.save
        render json: @current_user
      else
        render json: @current_user.errors, status: :unprocessable_entity
      end
    end

    # DELETE /users/1
    def destroy
      @user.destroy
    end

    #User HMO

    def myhmo
      if @current_user && @current_user.userhmo != nil
        render json: {
          userhmo_id: @current_user.userhmo.id,
          userhmo_isDeleted: @current_user.userhmo.isDeleted,
          hmo: @current_user.userhmo.hmo
        }
      else
        render json: { error: 404, status: :not_found }
      end
    end

    private
    # Only allow a trusted parameter "white list" through.
    def contact_number_params
      params.require(:contact_details).permit(:contact_number_id, :area_code, :number) if params[:contact_details]
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :birthdate)
    end

    def login_params
      params.permit(:email, :username, :password)
    end

    def doctor_params
      params.require(:doctor_details).permit(:license_id, :issue_date, :is_verified)
    end
  end
end

