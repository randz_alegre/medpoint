module V1 
  class SpecialtiesController < ApplicationController
    before_action :set_specialty, only: [:show, :update, :destroy]

    # GET /specialties
    def index
      @specialties = Specialty.all
      json_response @specialties
    end

    # POST /specialties
    def create
      if @current_user.is_admin?
        specialty = Specialty.create!(specialty_params)
        json_response({message: Message.specialty_created, specialty: specialty})
      else
       json_response({message: Message.not_allowed},:NotFound) 
      end
    end

    # GET /specialties/:id
    def show
      json_response(@specialty)
    end

    # PUT /specialties/:id
    def update
      if @current_user.is_admin?
        @specialty.update(specialty_params)
        json_response({message: Message.updated})
      else
        json_response({message: Message.not_allowed}, :NotFound)
      end
    end
    # DELETE /specialties/:id
    def destroy
      if @current_user.is_admin?
        @specialty.destroy
        json_response({message: Message.deleted})
      else
        json_response({message: Message.not_allowed}, :NotFound)
      end
      @specialty.destroy
    end

    private

    def specialty_params
      # whitelist params
      params.permit(:specialty_name, :specialty_desc)
    end

    def set_specialty
      @specialty = Specialty.find(params[:id])
    end
  end
end
