module V1 
  class VenueAdminController < ApplicationController
    def index
      if @current_user.is_admin?    
      end
    end
    def create
      if @current_user.is_admin?
       venue_admin = VenueAdmin.create!(venue_admin_params)
        json_response({message: Message.created, venue_admin: venue_admin})
      else
        json_response({message: Message.not_allowed},:NotFound) 
      end
    end
  end
end



