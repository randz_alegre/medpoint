class ApplicationController < ActionController::API
  include Pundit
  include Response
  include ExceptionHandler

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  rescue_from ActiveRecord::RecordNotUnique, :with => :record_exist
  rescue_from Pundit::NotAuthorizedError,  with: :user_not_authorized

  before_action :authorize_request
  attr_reader :current_user


  private

  def current_user
    @current_user ||= nil
  end

  def record_not_found
    render :json => { :error => "record not found"}, :status => 404
  end

  def record_exist
    render json: { :error => "record already exist"}, :status => 401
  end

  def user_not_authorized
    render json: { :error => "You are not Authorize to perform this action"}, :status => 403
  end

  # Check for valid request token and return user
  def authorize_request
    @current_user = (AuthorizeApiRequest.new(request.headers).call)[:user]
  end

  def token

    if request.headers['Authorization'].present?
      userToken = request.headers['Authorization'].split(' ').last

      Auth.decode(userToken)
    else
      false 
    end



  end

end
