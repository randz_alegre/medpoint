class Rack::Attack

	Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

	throttle('req/ip', :limit => 10, :period => 5.seconds) do |req|
    	req.ip # unless req.path.start_with?('/assets')
  	end

  	throttle('/v1/users/authenticatelogin/email', :limit => 10, :period => 60.seconds) do |req|
	    if req.path == '/v1/users/authenticatelogin' && req.post?
	      req.ip
	    end
  	end

end
