Rails.application.routes.draw do

  scope module: :v1, path: :v1 do

    resources :clinics, except: [:destroy] do
      resources :hmos, only: [:index]
      collection do
        get 'searchnearby'
        get 'searchbycity'
        get 'citylist'
        post 'importclinics'
        
      end
      resources :schedules 
      get 'getclinicrequests', to: 'get_clinic_requests'
      post 'approveclinicrequest', to: 'approve_clinic_request'
      get 'getschedulerequests', to: 'schedules#get_schedule_requests'
      post 'approveschedulerequest', to: 'schedules#approve_schedule_request'
      post 'addadmin', to: 'clinics#add_admin'
      delete 'removeadmin', to: 'clinics#remove_admin'
      get 'getadminrequests', to: 'clinics#get_admin_requests'
      patch 'approverequest', to: 'clinics#approve_admin_request'
      get 'admins', to: 'clinics#get_admins'
    end


    resources :subscriptions, only: [:index, :create, :update]

    resources :hmos, except: [:destroy] do
      resources :clinics,only: [:index]
      collection do
        post 'importhmos'
      end
    end

    resources :hmoclinics, only: [:create, :update]
    resources :userhmos, only: [:create, :update]

    resources :users, except: [:destroy] do
      resources :hmos, only: [:show, :index]
      collection do
        post 'authenticatelogin'
        get 'myhmo'
        #post 'forgotpassword'
        #post 'resetpassword/:token' => 'users#resetpassword'
        #post 'updatepassword'
        post 'iAmADoctor', to: 'users#upgrade_to_doctor'
        post 'contactNumber', to: 'users#add_contact_number'
        delete 'contactNumber', to: 'users#delete_contact_number'
        get 'contactNumbers', to: 'users#contact_numbers'
      end
    end
    post 'doctorprofile', to: 'doctors#index'
    delete 'deactivateprofile_d', to: 'doctors#destroy'

    resources :specialties
  end
  #TEMPORARY
  post 'auth/login', to: 'v1/authentication#authenticate'
  post 'signup', to: 'v1/users#create'
  post 'doctorsSignup', to: 'v1/users#create_doctor'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
