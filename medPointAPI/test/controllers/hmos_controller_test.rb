require 'test_helper'

class HmosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hmo = hmos(:one)
  end

  test "should get index" do
    get hmos_url, as: :json
    assert_response :success
  end

  test "should create hmo" do
    assert_difference('Hmo.count') do
      post hmos_url, params: { hmo: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show hmo" do
    get hmo_url(@hmo), as: :json
    assert_response :success
  end

  test "should update hmo" do
    patch hmo_url(@hmo), params: { hmo: {  } }, as: :json
    assert_response 200
  end

  test "should destroy hmo" do
    assert_difference('Hmo.count', -1) do
      delete hmo_url(@hmo), as: :json
    end

    assert_response 204
  end
end
