require 'test_helper'

class HmoClinicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hmo_clinic = hmo_clinics(:one)
  end

  test "should get index" do
    get hmo_clinics_url, as: :json
    assert_response :success
  end

  test "should create hmo_clinic" do
    assert_difference('HmoClinic.count') do
      post hmo_clinics_url, params: { hmo_clinic: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show hmo_clinic" do
    get hmo_clinic_url(@hmo_clinic), as: :json
    assert_response :success
  end

  test "should update hmo_clinic" do
    patch hmo_clinic_url(@hmo_clinic), params: { hmo_clinic: {  } }, as: :json
    assert_response 200
  end

  test "should destroy hmo_clinic" do
    assert_difference('HmoClinic.count', -1) do
      delete hmo_clinic_url(@hmo_clinic), as: :json
    end

    assert_response 204
  end
end
