require 'test_helper'

class HmoclinicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hmoclinic = hmoclinics(:one)
  end

  test "should get index" do
    get hmoclinics_url, as: :json
    assert_response :success
  end

  test "should create hmoclinic" do
    assert_difference('Hmoclinic.count') do
      post hmoclinics_url, params: { hmoclinic: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show hmoclinic" do
    get hmoclinic_url(@hmoclinic), as: :json
    assert_response :success
  end

  test "should update hmoclinic" do
    patch hmoclinic_url(@hmoclinic), params: { hmoclinic: {  } }, as: :json
    assert_response 200
  end

  test "should destroy hmoclinic" do
    assert_difference('Hmoclinic.count', -1) do
      delete hmoclinic_url(@hmoclinic), as: :json
    end

    assert_response 204
  end
end
