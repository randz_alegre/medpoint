# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Hmo.delete_all
Specialty.delete_all
LoginCredential.delete_all
ContactNumber.delete_all
User.delete_all

Hmo.create!(
	hmo_name: "Asian life Insurance", 
	hmo_contact_number: "917- 5031713;0917- 5133718", 
	hmo_website: "http://www.asianlife.com.ph/",
	hmo_hotline: "+63 (2) 811-1313;+63 (2) 865-3700",
	hmo_logo: "test",
	complete_address: "Test rapd"
)

Hmo.create!(
	hmo_name: "Fortune Care", 
	hmo_contact_number: "917- 5031713;0917- 5133718", 
	hmo_website: "http://www.fortunecare.com.ph/",
	hmo_hotline: "+63 (2) 811-1313;+63 (2) 865-3700",
	hmo_logo: "test",
	complete_address: "Test rapd"
)

Hmo.create!(
	hmo_name: "Caritas Health Shield", 
	hmo_contact_number: "(02) 868-7000", 
	hmo_website: "https://www.caritashealthshield.com.ph/",
	hmo_hotline: "0917-8733882;0917-8733880;0917-873318",
	hmo_logo: "test",
	complete_address: "Test rapd"
)

Hmo.create!(
	hmo_name: "Insular Health Care", 
	hmo_contact_number: "test", 
	hmo_website: "test",
	hmo_hotline: "test",
	hmo_logo: "test",
	complete_address: "test"
)

Hmo.create!(
	hmo_name: "Kaiser International Health Group", 
	hmo_contact_number: "(632) 892-9634;(632) 892-9636", 
	hmo_website: "http://www.kaiserhealthgroup.com/",
	hmo_hotline: "(02) 274 8202;(02) 274 8203;(02) 274 8205",
	hmo_logo: "test",
	complete_address: "G/F King's Court I Bldg., 2129 Chino Roces Ave., Makati City 1200, Philippines"
)

Hmo.create!(
	hmo_name: "Life and Health HMP Incorporated", 
	hmo_contact_number: "test", 
	hmo_website: "test",
	hmo_hotline: "test",
	hmo_logo: "test",
	complete_address: "test"
)

Hmo.create!(
	hmo_name: "Intellicare", 
	hmo_contact_number: "(02) 789-4000", 
	hmo_website: "https://www.intellicare.com.ph",
	hmo_hotline: "test",
	hmo_logo: "test",
	complete_address: "7th Floor, Feliza Building, 108 V.A. Rufino Street (formerly Herrera Street),Legaspi Village, Makati City, Philippines 1229"
)

Hmo.create!(
	hmo_name: "carehealthplus", 
	hmo_contact_number: "(+63) 02 521 9927", 
	hmo_website: "http://www.carehealthplus.com/",
	hmo_hotline: "(02) 208 4611;0926 702 8360 ; 0927 8042137;0925 6021927",
	hmo_logo: "test",
	complete_address: "Suite 905, 9th Floor, L & S Building 1414 Roxas Blvd., Ermita, Manila"
)

user = User.create!(
  first_name: "Kevin",
  last_name: "Pineda",
  birthdate: "1992-11-27",
  user_type: "xxxAdmin-Medpoint-Personel",
  is_verified: true
)

LoginCredential.create!(
  user_id: user.id,
  email: "kevinpineda@gmail.com",
  password: "admin",
  username: "kevinp"
)

ContactNumber.create!(
  user_id: user.id,
  area_code: "+632",
  number: "9995151008"
)

user = User.create!(
  first_name: "Vincent",
  last_name: "Pacul",
  birthdate: "1992-12-25",
  user_type: "user",
  is_verified: false
)

LoginCredential.create!(
  user_id: user.id,
  email: "vincentpacul@gmail.com",
  password: "admin",
  username: "vincentp"
)

ContactNumber.create!(
  user_id: user.id,
  area_code: "+632",
  number: "9228588179"
)

ContactNumber.create!(
  user_id: user.id,
  area_code: "+632",
  number: "49494949i"
)

user = User.create!(
  first_name: "Randolf",
  last_name: "Alegre",
  birthdate: "1992-01-01",
  user_type: "user",
  is_verified: false
)

LoginCredential.create!(
  user_id: user.id,
  email: "randolfa@gmail.com",
  password: "admin",
  username: "randolfa"
)

ContactNumber.create!(
  user_id: user.id,
  area_code: "+632",
  number: "955555555"
)

Doctor.create!(
  user_id: user.id,
  license_id: "696969",
  issue_date:"2015-01-01"
)












