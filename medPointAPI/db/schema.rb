# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171203110157) do

  create_table "appointments", force: :cascade do |t|
    t.integer "schedule_id"
    t.integer "user_id"
    t.integer "status", default: 1, null: false
    t.datetime "proposed_schedule", null: false
    t.index ["schedule_id"], name: "index_appointments_on_schedule_id"
    t.index ["user_id"], name: "index_appointments_on_user_id"
  end

  create_table "clinics", force: :cascade do |t|
    t.string "clinic_name", null: false
    t.string "city", null: false
    t.string "province", null: false
    t.string "telephone_number", null: false
    t.string "mobile_number", null: false
    t.float "latitude", null: false
    t.float "longitude", null: false
    t.string "street_address", null: false
    t.string "floor_level", null: false
    t.string "bldg_name", null: false
    t.string "barangay", null: false
    t.string "establishment_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "coordinator", default: "", null: false
    t.string "asst_coordinator", default: "", null: false
    t.boolean "is_approved", default: false
  end

  create_table "contact_numbers", force: :cascade do |t|
    t.string "area_code", null: false
    t.string "number", null: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contact_numbers_on_user_id"
  end

  create_table "doctors", force: :cascade do |t|
    t.string "license_id", default: "", null: false
    t.date "issue_date", null: false
    t.boolean "is_verified", default: false, null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_doctors_on_user_id"
  end

  create_table "doctors_specialties", id: false, force: :cascade do |t|
    t.integer "doctor_id", null: false
    t.integer "specialty_id", null: false
    t.string "certification_id", null: false
    t.date "certification_date", null: false
  end

  create_table "hmoclinics", force: :cascade do |t|
    t.integer "clinic_id"
    t.integer "hmo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clinic_id", "hmo_id"], name: "index_hmoclinics_on_clinic_id_and_hmo_id", unique: true
    t.index ["clinic_id"], name: "index_hmoclinics_on_clinic_id"
    t.index ["hmo_id"], name: "index_hmoclinics_on_hmo_id"
  end

  create_table "hmos", force: :cascade do |t|
    t.string "hmo_name", null: false
    t.string "hmo_contact_number", null: false
    t.string "hmo_website", null: false
    t.string "hmo_hotline", null: false
    t.string "hmo_logo", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "complete_address"
  end

  create_table "login_credentials", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "password_digest", default: "", null: false
    t.string "username", default: "", null: false
    t.string "password_hash", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_login_credentials_on_user_id"
  end

  create_table "patient_histories", force: :cascade do |t|
    t.integer "appointment_id"
    t.integer "user_id"
    t.string "diagnosis"
    t.date "recommended_next_schedule"
    t.index ["appointment_id"], name: "index_patient_histories_on_appointment_id"
    t.index ["user_id"], name: "index_patient_histories_on_user_id"
  end

  create_table "prescriptions", force: :cascade do |t|
    t.integer "appointment_id"
    t.integer "user_id"
    t.string "medicine_name", null: false
    t.string "frequency", default: "1", null: false
    t.integer "dosagePerFrequency", null: false
    t.string "comments"
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.index ["appointment_id"], name: "index_prescriptions_on_appointment_id"
    t.index ["user_id"], name: "index_prescriptions_on_user_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.integer "clinic_id"
    t.integer "doctor_id"
    t.integer "frequency", default: 1
    t.time "start_time", null: false
    t.time "end_time", null: false
    t.integer "timezone", null: false
    t.boolean "isActive", default: false
    t.index ["clinic_id"], name: "index_schedules_on_clinic_id"
    t.index ["doctor_id"], name: "index_schedules_on_doctor_id"
  end

  create_table "specialties", force: :cascade do |t|
    t.string "specialty_name", null: false
    t.string "specialty_desc", null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "userhmos", force: :cascade do |t|
    t.integer "user_id"
    t.integer "hmo_id"
    t.boolean "isDeleted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hmo_id"], name: "index_userhmos_on_hmo_id"
    t.index ["user_id", "hmo_id"], name: "index_userhmos_on_user_id_and_hmo_id", unique: true
    t.index ["user_id"], name: "index_userhmos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.date "birthdate"
    t.string "user_type", default: "user"
    t.boolean "is_verified", default: false
  end

  create_table "venue_admins", force: :cascade do |t|
    t.integer "user_id"
    t.integer "clinic_id"
    t.boolean "is_approved"
    t.index ["clinic_id"], name: "index_venue_admins_on_clinic_id"
    t.index ["user_id"], name: "index_venue_admins_on_user_id"
  end

end
