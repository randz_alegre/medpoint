class AddNewColumnToClinicsTable < ActiveRecord::Migration[5.1]
  def change
  	add_column	:clinics, :street_address, :string
	add_column 	:clinics, :floor_level, :string
  	add_column 	:clinics, :bldg_name, :string
  	add_column 	:clinics, :barangay, :string
  end
end
