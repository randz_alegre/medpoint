class ChangeLoginCredentialFieldName < ActiveRecord::Migration[5.1]
  def change
	rename_column :login_credentials, :password, :password_digest
  end
end
