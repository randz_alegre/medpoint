class CreateJoinTableDoctorSpecialty < ActiveRecord::Migration[5.1]
  def change
    create_join_table :doctors, :specialties do |t|
      # t.index [:doctor_id, :specialty_id]
      # t.index [:specialty_id, :doctor_id]
	  t.string :certification_id, null: false
	  t.date :certification_date, null: false
	  # Should we store image of the certification 
	  
    end
  end
end
