class AddNewColumnForClinicTable < ActiveRecord::Migration[5.1]
  def change
  	drop_table :clinics, force: :cascade
  end
end
