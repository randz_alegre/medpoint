class CreateHmoclinics < ActiveRecord::Migration[5.1]
  def change
    create_table :hmoclinics do |t|
    	t.references :clinic, foreign_key: true
    	t.references :hmo, foreign_key: true
      	t.timestamps
    end
  end
end
