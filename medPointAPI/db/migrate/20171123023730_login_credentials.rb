class LoginCredentials < ActiveRecord::Migration[5.1]
  def change
   create_table :login_credentials do |t|
      t.string :email, null: false, default: ""
	  t.string :password, null: false, default: ""
	  t.string :username, null: false, default: ""
	  t.string :password_hash, null: false, default: ""
	  t.string :reset_password_token
	  t.datetime :reset_password_sent_at
	  t.references :user, foreign_key: true
	  
      t.timestamps
    end
  end
end
