class CreateHmos < ActiveRecord::Migration[5.1]
  def change
    create_table :hmos do |t|
      t.string :hmo_name, null: false
      t.string :hmo_contact_number, null: false
      t.string :hmo_website, null: false
      t.string :hmo_hotline, null: false
      t.string :hmo_logo, null: false

      t.timestamps
    end
  end
end
