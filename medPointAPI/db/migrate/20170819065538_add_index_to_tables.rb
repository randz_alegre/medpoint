class AddIndexToTables < ActiveRecord::Migration[5.1]
  def change
  	add_index :hmoclinics, [:clinic_id, :hmo_id], unique: true
  	add_index :userhmos, [:user_id, :hmo_id], unique: true
  end
end
