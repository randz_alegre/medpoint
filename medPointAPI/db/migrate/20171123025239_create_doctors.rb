class CreateDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :doctors do |t|
		t.string :license_id, null: false, default: ""
		t.date :issue_date, null: false
		t.boolean :is_verified, null: false, default: false
    end
  end
end
