class UpdateClinicTable < ActiveRecord::Migration[5.1]
  def change
  	add_column :clinics, :coordinator, :string, null: false, default: ""
  	add_column :clinics, :asst_coordinator, :string, null: false, default: ""
  	remove_column :clinics, :description, :string
  end
end
