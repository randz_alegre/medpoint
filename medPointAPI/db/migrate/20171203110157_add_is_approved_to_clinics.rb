class AddIsApprovedToClinics < ActiveRecord::Migration[5.1]
  def change
    add_column :clinics, :is_approved, :boolean, default: false
  end
end
