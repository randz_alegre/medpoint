class AddTableForUserHmo < ActiveRecord::Migration[5.1]
  def change
  	create_table :userhmo do |t|
    	t.references :user, foreign_key: true
    	t.references :hmo, foreign_key: true
    	t.boolean :isDeleted, default: false
      	t.timestamps
    end
  end
end
