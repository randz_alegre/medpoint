class AddNewClinicTable < ActiveRecord::Migration[5.1]
  def change
    create_table :clinics do |t|
      	t.string :clinic_name, null: false
		t.string :city, null: false
		t.string :province, null: false
		t.string :telephone_number, null: false
		t.string :mobile_number, null: false
		t.text 	 :description, null: false
		t.float :latitude, null: false
		t.float :longitude, null: false
		t.string :street_address, null: false
		t.string :floor_level, null: false
		t.string :bldg_name, null: false
		t.string :barangay, null: false
		t.string :establishment_type, null: false
		t.timestamps		
    end
  end
end
