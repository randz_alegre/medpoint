class ContactNumber < ActiveRecord::Migration[5.1]
  def change
   create_table :contact_numbers do |t|
      t.string :area_code, null: false
	  t.string :number, null: false
	  t.references :user, foreign_key: true
	  
      t.timestamps
    end
  end
end
