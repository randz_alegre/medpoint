class CreatePatientHistory < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_histories do |t|
      t.references :appointment, foreign_key: true
	  t.references :user, foreign_key: true
	  t.string :diagnosis
	  t.date :recommended_next_schedule
    end
  end
end
