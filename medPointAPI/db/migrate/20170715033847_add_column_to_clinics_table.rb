class AddColumnToClinicsTable < ActiveRecord::Migration[5.1]
  def change
  	remove_column :clinics, :address, :string
  end
end
