class RemoveEmailPasswordDigestAndCellphoneNumberFromUsers < ActiveRecord::Migration[5.1]
  def change 
	remove_column :users, :email, :string
	remove_column :users, :password_digest, :string
	remove_column :users, :cellphone_number, :string
	remove_column :users, :reset_password_token, :string
	remove_column :users, :reset_password_sent_at, :datetime
  end
end
