class CreatePrescription < ActiveRecord::Migration[5.1]
  def change
    create_table :prescriptions do |t|
		t.references :appointment, foreign_key: true
		t.references :user, foreign_key: true
		t.string :medicine_name, null: false
		#1-daily,2-weekly or depende sa sabot
		t.string :frequency, null: false, default: 1
		t.integer :dosagePerFrequency, null: false
		t.string :comments
		t.date :start_date, null: false
		t.date :end_date, null: false
    end
  end
end
