class CreateSpecialties < ActiveRecord::Migration[5.1]
  def change
    create_table :specialties do |t|
	  t.string :specialty_name, null: false
	  t.string :specialty_desc, null: false
    end
  end
end
