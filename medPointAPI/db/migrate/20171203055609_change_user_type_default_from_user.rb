class ChangeUserTypeDefaultFromUser < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :user_type, :string, default: "user"
  end
end
