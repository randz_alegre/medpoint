class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
	t.references :schedule, foreign_key: true
	t.references :user, foreign_key: true
	#Status 1-Pending, 2-Approved, 3-Cancelled, and etc
	t.integer :status, null: false, default: 1
	t.datetime :proposed_schedule, null: false
    end
  end
end
