class AddNewColumnToClinicTable < ActiveRecord::Migration[5.1]
  def change
  	add_column :clinics, :establishment_type, :string
  	add_column :users, :cellphone_number, :string
  end
end
