class CreateVenueAdmin < ActiveRecord::Migration[5.1]
  def change
    create_table :venue_admins do |t|
      t.references :user, foreign_key: true
      t.references :clinic, foreign_key: true
      t.boolean :is_approved
    end
  end
end
