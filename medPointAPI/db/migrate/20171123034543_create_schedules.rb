class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
	t.references :clinic, foreign_key: true
	t.references :doctor, foreign_key: true
	#Frequency 1- weekly, 2- monthly, 3-quarterly, 4-yearly
	t.integer :frequency, default: 1
	t.time :start_time, null: false
	t.time :end_time, null: false
	#So that time inputted is based on the timezone from -12 to +12
	t.integer :timezone, null: false
	# isActive to indicate if the schedule still applies
	t.boolean :isActive, default: false
    end
  end
end
